/*
 * Copyright 2019 LAAS-CNRS
 * MESSIOUX Antonin / FAVIER Anthony
 *
 * This file is part of the OSMOSIS project.
 *
 * Osmosis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Osmosis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */

#include <ros/ros.h>
#include <osmosis_control/teleop_joy.hpp>

using namespace std;

class JoyTeleop : public osmosis::TeleopJoy
{
private:
	///////// Attributes ////////
	ros::NodeHandle nh_;
	double freq_;
	sensor_msgs::Joy joy_msg_;
	osmosis_control_msgs::Teleop joy_teleop_cmd_;
	ros::Publisher cmd_joy_teleop_pub_;
	ros::Subscriber cmd_joystick_sub_;

public:
	JoyTeleop() : osmosis::TeleopJoy()
	{
		freq_=10;
		cmd_joy_teleop_pub_ = nh_.advertise<osmosis_control_msgs::Teleop>("cmd_vel", 1);
		cmd_joystick_sub_= nh_.subscribe("joy", 1, &JoyTeleop::teleopCallbackJoy, this);
	}

	void run()
	{
		ROS_INFO("You should now be able to activate the controller (with 'start')");
		ros::Rate loop_rate(freq_);
		while (nh_.ok())
		{
			ROS_INFO_STREAM("JoyTeleop state: " << get_state());
			step(joy_msg_, joy_teleop_cmd_);
			ROS_INFO_STREAM("  -> " << get_state());
			cmd_joy_teleop_pub_.publish(joy_teleop_cmd_);
		 	ros::spinOnce(); // Need to call this function often to allow ROS to process incoming messages
			loop_rate.sleep(); // Sleep for the rest of the cycle, to enforce the loop rate
		}
	}

	void teleopCallbackJoy(const sensor_msgs::Joy & joy_msg)
	{
		joy_msg_ = joy_msg;
	}


}; // end of class

////////////////////// MAIN //////////////////////

int main(int argc, char** argv)
{
	//init the ROS node
	ros::init(argc, argv, "joy_teleop_node");

	JoyTeleop myJoyTeleop;
	myJoyTeleop.run();
}
