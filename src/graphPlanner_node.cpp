/*
* Copyright 2018 LAAS-CNRS
*
* This file is part of the OSMOSIS project.
*
* Osmosis is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 3 as
* published by the Free Software Foundation.
*
* Osmosis is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
*/

#include <ros/ros.h>
#include <ros/package.h>
#include <osmosis_control/graphPlanner.hpp>

using namespace std;

class GraphPlanner : public osmosis::GraphPlanner
{
private:
	///////// Attributes ////////
	ros::NodeHandle nh_;
	double freq_;

	ros::Publisher target_pub_;
	ros::Publisher goal_reached_pub_;
	ros::Subscriber goal_sub_;
	ros::Subscriber target_reached_sub_;
	ros::Subscriber odom_sub_;

	geometry_msgs::Point current;
	osmosis_control_msgs::Goal target_;
	osmosis_control_msgs::Goal mission_goal_;
	osmosis::Graph graph;
	std::vector<geometry_msgs::Point> plan;
	bool _new_goal;
	bool target_reached_;

public:
	GraphPlanner() : osmosis::GraphPlanner(), nh_() {
		freq_=10;
		//set up the publisher for the goal topic
		target_pub_ = nh_.advertise<osmosis_control_msgs::Goal>("target", 1);
		goal_reached_pub_ = nh_.advertise<std_msgs::Bool>("goal_reached", 1);
		goal_sub_=nh_.subscribe("mission_goal", 1, &GraphPlanner::callbackGoal, this);
		odom_sub_=nh_.subscribe("pose", 1, &GraphPlanner::callbackPose, this);
		target_reached_sub_=nh_.subscribe("target_reached", 1, &GraphPlanner::callbackTargetReached, this);
		//
		_new_goal=false;
		target_reached_=false;
		//
		std::string graph_path;
		nh_.param<std::string>("graph", graph_path, "ressources/graph/blagnac.graph");
		initGraph(graph_path, graph);
		ROS_INFO_STREAM("Initialized graph from " << graph_path << " (" << graph.size() << ")");
	}

	void run() {
		ros::Rate loop_rate(freq_); //using 10 makes the robot oscillating trajectories, TBD check with the PF algo ?
		while (nh_.ok())
		{
			ROS_INFO_STREAM("GraphPlanner state " << get_state_str());
			step(graph, current, mission_goal_.point, plan, _new_goal, target_reached_);
			ROS_INFO_STREAM("  -> " << get_state_str());
			if (get_state() == State::SEND_NEXT)
				publishSendTarget();
			if (get_state() == FOLLOW) {
				ROS_INFO("POSE x: %f  y:%f", current.x , current.y );
				ROS_INFO("PLAN x: %f  y:%f", plan[target_index].x , plan[target_index].y );
				ROS_INFO("dist to target = %f", osmosis::Graph::distance(current, plan[target_index]));
			}
			if (get_state() == GOAL_DONE)
				publishDone();
			ros::spinOnce(); // Need to call this function often to allow ROS to process incoming messages
			loop_rate.sleep(); // Sleep for the rest of the cycle, to enforce the loop rate
		}
	}

	void callbackGoal(const osmosis_control_msgs::Goal & thegoal) {
		mission_goal_=thegoal;
		_new_goal=true;
		ROS_INFO("NEW GOAL : x: [%f], y:[%f]",mission_goal_.point.x,mission_goal_.point.y);
	}

	void callbackPose(const geometry_msgs::Pose2D & msg) {
		current.x = msg.x;
		current.y = msg.y;
		//ROS_INFO("NEW POS : x: [%f], y:[%f]",current.x,current.y);
	}

	void callbackTargetReached(const std_msgs::Bool & target_reached) {
		target_reached_=target_reached.data;
		ROS_INFO("Target reached : [%d]",target_reached_);
	}


	void publishDone(){
		std_msgs::Bool target_reached;
		target_reached.data=true;;
		goal_reached_pub_.publish(target_reached);
	}

	void publishSendTarget() {
		ROS_INFO("SEND target x: %f y:%f", plan[target_index].x, plan[target_index].y);
		target_.point=plan[target_index];
		target_.taxi=mission_goal_.taxi;
		target_pub_.publish(target_);
	}

}; // end of class

////////////////////// MAIN //////////////////////

int main(int argc, char** argv)
{
	//init the ROS node
	ros::init(argc, argv, "graph_planner_node");

	GraphPlanner myGraphPlanner;
	myGraphPlanner.run();
}
