/*
 * Copyright 2019 LAAS-CNRS
 * MESSIOUX Antonin / FAVIER Anthony
 *
 * This file is part of the OSMOSIS project.
 *
 * Osmosis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Osmosis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */

#include <iostream>
#include <ros/ros.h>
#include <osmosis_control/teleop.hpp>

using namespace std;

class Teleop : public osmosis::Teleop
{
private:
	///////// Attributes ////////
	ros::NodeHandle nh_;
	double freq_;
	ros::Publisher cmd_teleop_pub_;
	osmosis_control_msgs::Teleop teleop_cmd_;

public:
	Teleop() : osmosis::Teleop()
  {
  	freq_=10;
  	cmd_teleop_pub_ = nh_.advertise<osmosis_control_msgs::Teleop>("cmd_vel", 1);
  	teleop_cmd_.is_active=false;
  	teleop_cmd_.cmd_vel.linear.x=teleop_cmd_.cmd_vel.linear.y=teleop_cmd_.cmd_vel.angular.z=0;
  }

	void run()
  {
  	ROS_INFO("Type a command and then press enter.\nUse '1' to activate the telecommand\nthen'+' to move forward,\n'l' to turn left,\n'r' to turn right,\n'.' to exit.");
		std::string cmd;

  	ros::Rate loop_rate(freq_);
  	while (nh_.ok())
  	{
      std::getline(std::cin, cmd);
			ROS_INFO_STREAM("Teleop state: " << get_state());
			step(cmd, teleop_cmd_);
      ROS_INFO_STREAM("  -> " << get_state());
  		cmd_teleop_pub_.publish(teleop_cmd_);
  		loop_rate.sleep(); // Sleep for the rest of the cycle, to enforce the loop rate
  	}
  }

}; // end of class

////////////////////// MAIN //////////////////////

int main(int argc, char** argv)
{
	//init the ROS node
	ros::init(argc, argv, "teleop_node");

	Teleop myTeleop;
	myTeleop.run();
}
