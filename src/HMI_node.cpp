/*
 * Copyright 2019 LAAS-CNRS
 * MESSIOUX Antonin / FAVIER Anthony
 *
 * This file is part of the OSMOSIS project.
 *
 * Osmosis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Osmosis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */

#include <ros/ros.h>
#include <ros/package.h>
#include <osmosis_control/HMI.hpp>

using namespace std;

class HMI : public osmosis::HMI
{
private:
	///////// Attributes /////////
	ros::NodeHandle nh_;
	double freq_;
	ros::Publisher mission_pub_;
	ros::Subscriber done_sub_;

	osmosis_control_msgs::Mission mission_cmd_;
	std::string filename;

	bool goal_reached_;
	bool mission_done_;

	void publishMission();

public:
	HMI() : osmosis::HMI() {
		freq_=10;
		mission_pub_ = nh_.advertise<osmosis_control_msgs::Mission>("mission", 1);
		done_sub_ = nh_.subscribe("hmi_done", 1, &HMI::callbackMissionDone, this);
		mission_done_=true;
		//filename=ros::package::getPath("osmosis_control");
		nh_.param<std::string>("missions", filename, "ressources/missions/");
		ROS_INFO_STREAM("Mission folder: " << filename);
	}

	void run() {
		ros::Rate loop_rate(freq_); //using 10 makes the robot oscillating trajectories, TBD check with the PF algo ?
		while (nh_.ok())
		{
			auto prev_state = get_state_str();
			step(mission_cmd_, mission_done_, filename);
			auto next_state = get_state_str();
			if (prev_state != next_state)
				ROS_INFO_STREAM("HMI " << prev_state << " -> " << next_state);
			if (get_state_mission() == StateMission::START_MISSION)
				mission_pub_.publish(mission_cmd_);
		 	ros::spinOnce(); // Need to call this function often to allow ROS to process incoming messages
			loop_rate.sleep(); // Sleep for the rest of the cycle, to enforce the loop rate
		}
	}

	void callbackMissionDone(const std_msgs::Bool &done) {
		mission_done_= done.data;
	}

};

////////////////////// MAIN //////////////////////

int main(int argc, char** argv)
{
	//init the ROS node
	ros::init(argc, argv, "HMI_node");

	HMI myHMI;
	myHMI.run();
}
