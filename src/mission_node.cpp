/*
 * Copyright 2019 LAAS-CNRS
 * MESSIOUX Antonin / FAVIER Anthony
 *
 * This file is part of the OSMOSIS project.
 *
 * Osmosis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Osmosis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */

#include <ros/ros.h>
#include <std_msgs/Bool.h>
#include <osmosis_control_msgs/Mission.h>
#include <osmosis_control/mission.hpp>

#include <ros/package.h>
using namespace std;

class MissionManager : public osmosis::MissionManager
{
private:
	///////// Attributes ////////
	ros::NodeHandle nh_;
	ros::Publisher goal_pub_;
	ros::Publisher hmi_done_pub_;
	ros::Subscriber goal_reached_sub_;
	ros::Subscriber hmi_mission_sub_;

	osmosis_control_msgs::Mission mission_msg_;
  std::string mission_folder;
	bool mission_received_=true;
  bool goal_reached_;

  osmosis_control_msgs::Goal goal_cmd_;
	std_msgs::Bool done_;

  void callbackGoalReached(const std_msgs::Bool &goal_reached)
  {
  	goal_reached_ = goal_reached.data;
  }

	void callbackMission(const osmosis_control_msgs::Mission &mission)
  {
  	mission_received_ = true;
  	mission_msg_ = mission;
  }

public:
	MissionManager() : osmosis::MissionManager() {
    //set up the publisher for the goal topic
  	goal_pub_ = nh_.advertise<osmosis_control_msgs::Goal>("mission_goal", 1);
  	hmi_done_pub_ = nh_.advertise<std_msgs::Bool>("hmi_done", 1);
  	goal_reached_sub_ = nh_.subscribe("goal_reached", 1, &MissionManager::callbackGoalReached, this);
  	hmi_mission_sub_ = nh_.subscribe("mission", 1, &MissionManager::callbackMission, this);

  	goal_reached_=false;
  	goal_cmd_.taxi=true;
  	mission_received_=false;

		nh_.param<std::string>("missions", mission_folder, "ressources/missions/");
		ROS_INFO_STREAM("Mission folder: " << mission_folder);
    //mission_folder = ros::package::getPath("osmosis_control") + "/ressources/missions";
  }

	void run() {
  	ros::Rate loop_rate(10); //using 10 makes the robot oscillating trajectories, TBD check with the PF algo ?
  	while (nh_.ok())
  	{
			auto prev_state = get_state_str();
  		step(mission_received_, goal_reached_, mission_msg_, done_,
        goal_cmd_, mission_folder);
			auto next_state = get_state_str();
			if (next_state != prev_state)
      	ROS_INFO_STREAM("Mission " << prev_state << " -> " << next_state);
      if (get_state_task() == StateTask::SEND_MISSION) {
				ROS_INFO_STREAM("Step " << mission_step << ": " << goal_cmd_);
      	goal_pub_.publish(goal_cmd_);
			}
      if (get_state() == State::MISSION_DONE)
        hmi_done_pub_.publish(done_);
  		ros::spinOnce(); // Need to call this function often to allow ROS to process incoming messages
  		loop_rate.sleep(); // Sleep for the rest of the cycle, to enforce the loop rate
  	}
  }

}; // end of class

////////////////////// MAIN  //////////////////////

int main(int argc, char** argv)
{
	//init the ROS node
	ros::init(argc, argv, "mission_manager_node");

	MissionManager myMManager;
	myMManager.run();
}
