/*
 * Copyright 2018 LAAS-CNRS
 *
 * This file is part of the OSMOSIS project.
 *
 * Osmosis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Osmosis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */

#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/LaserScan.h>
#include <std_msgs/Bool.h>
#include <osmosis_control/safetyPilot.hpp>

class SafetyPilot : osmosis::SafetyPilot
{
private:
	///////// Attributes ////////
	ros::NodeHandle nh_;
	double freq_;

	ros::Publisher cmd_vel_pub_;

	ros::Subscriber cmd_vel_sub_;
	ros::Subscriber scan_sub_;
	ros::Subscriber cmd_vel_teleop_sub_;
	ros::Subscriber emergency_stop_sub_;
	ros::Subscriber controlled_stop_sub_;
	ros::Subscriber switch_to_teleop_sub_;
	ros::Subscriber fault_wrong_value_sub_, fault_not_updated_sub_;

	geometry_msgs::Twist base_cmd_ctrl_;
	geometry_msgs::Twist base_cmd_;
	osmosis_control_msgs::Teleop base_cmd_teleop_;
	sensor_msgs::LaserScan scan_;
	bool controlled_stop_, switch_teleop_;
	bool fault_wrong_value_, fault_not_updated_;

	double stop_distance;
	double stop_lateral_distance;
	double max_linear;
	double max_angular;

	void stop()
	{
		base_cmd_.linear.x=0;
		base_cmd_.angular.z=0;
	}

public:
	SafetyPilot()
		: osmosis::SafetyPilot()
		, freq_(10)
		, fault_not_updated_(false)
		, fault_wrong_value_(false)
		, controlled_stop_(false)
		, switch_teleop_(false)
	{
		//set up the publisher for the cmd_vel topic
		cmd_vel_pub_ = nh_.advertise<geometry_msgs::Twist>("cmd_vel", 1);
		cmd_vel_sub_  = nh_.subscribe("cmd_vel_control", 1, &SafetyPilot::callbackCmdVelCtrl, this);
		scan_sub_  = nh_.subscribe("scan", 1, &SafetyPilot::callbackScan, this);
		cmd_vel_teleop_sub_= nh_.subscribe("cmd_vel_teleop", 1, &SafetyPilot::callbackTeleop, this);
		controlled_stop_sub_ = nh_.subscribe("do_controlled_stop", 1, &SafetyPilot::callbackControlledStop, this);
		switch_to_teleop_sub_ = nh_.subscribe("do_switch_to_teleop", 1, &SafetyPilot::callbackSwitchTeleop, this);
		fault_not_updated_sub_ = nh_.subscribe("fault_not_updated", 1, &SafetyPilot::callbackFaultNotUpdated, this);
		fault_wrong_value_sub_ = nh_.subscribe("fault_wrong_value", 1, &SafetyPilot::callbackFaultWrong, this);
		// params
		nh_.param<double>("stop_distance", stop_distance, 0.8);
		nh_.param<double>("stop_lateral_distance", stop_lateral_distance, 0.3);
		nh_.param<double>("max_linear", max_linear, 1.0);
		nh_.param<double>("max_angular", max_angular, 2.0);
		ROS_INFO_STREAM("Parameters: stop_distance=" << stop_distance << " - stop_lateral_distance=" << stop_lateral_distance
			<< " - max_linear=" << max_linear << " - max_angular=" << max_angular);
	}

	bool run()
	{
		ros::Rate loop_rate(freq_);
		int fault_counter = 5;
		while (nh_.ok())
		{
			std::string prev_state = get_state();
			// intermittent fault
			if (fault_wrong_value_) {
				fault_counter--;
				if (fault_counter < 0) {
					fault_counter = 5;
					fault_wrong_value_ = false;
				}
			}
			//
			step(base_cmd_teleop_, base_cmd_ctrl_, base_cmd_,
				scan_, stop_lateral_distance, stop_distance, max_linear, max_angular,
				controlled_stop_, switch_teleop_, fault_wrong_value_);
			std::string curr_state = get_state();
			if (prev_state != curr_state)
				ROS_INFO_STREAM("safety state " << prev_state << " -> " << curr_state);
			ROS_DEBUG_STREAM("=> " << base_cmd_);
			if (! fault_not_updated_)
				cmd_vel_pub_.publish(base_cmd_);
			ros::spinOnce(); // Need to call this function often to allow ROS to process incoming messages
			loop_rate.sleep(); // Sleep for the rest of the cycle, to enforce the loop rate
		}

		return true;
	}

	void callbackCmdVelCtrl(const geometry_msgs::Twist & cmd_msg)
	{
		base_cmd_ctrl_ = cmd_msg;
	}

	void callbackScan(const sensor_msgs::LaserScan & scan_msg)
	{
		scan_=scan_msg;
	}

	void callbackTeleop(const osmosis_control_msgs::Teleop & teleop_msg)
	{
		ROS_INFO_STREAM("Received teleop " << teleop_msg.is_active);
		base_cmd_teleop_=teleop_msg;
	}

	void callbackControlledStop(const std_msgs::Bool& controlled_stop) {
		controlled_stop_ = controlled_stop.data;
	}

	void callbackSwitchTeleop(const std_msgs::Bool& switch_teleop) {
		switch_teleop_ = switch_teleop.data;
	}

	void callbackFaultWrong(const std_msgs::Bool& fault) {
		fault_wrong_value_ = fault.data;
	}

	void callbackFaultNotUpdated(const std_msgs::Bool& fault) {
		fault_not_updated_ = fault.data;
	}

}; // end of class

////////////////////// MAIN //////////////////////

int main(int argc, char** argv)
{
	//init the ROS node
	ros::init(argc, argv, "safety_pilot_node");

	SafetyPilot mySafePilot;
	mySafePilot.run();
}
