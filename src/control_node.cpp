/*
* Copyright 2018 LAAS-CNRS
*
* This file is part of the OSMOSIS project.
*
* Osmosis is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 3 as
* published by the Free Software Foundation.
*
* Osmosis is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
*/

#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Point.h>
//#include <geometry_msgs/Pose.h>
//#include <nav_msgs/Odometry.h>
#include <sensor_msgs/LaserScan.h>
#include <geometry_msgs/Pose2D.h>
#include <std_msgs/Bool.h>
#include <osmosis_control/control.hpp>

using namespace std;

class OsmosisControl : public osmosis::Control
{
private:
	///////// Attributes ////////
	ros::NodeHandle nh_;
	double freq_;
	ros::Publisher cmd_vel_pub_;
	ros::Publisher goal_reach_pub_;

	ros::Subscriber odom_sub_;
	ros::Subscriber scan_sub_;
	ros::Subscriber goal_sub_;

	geometry_msgs::Pose2D robot_pose_;
	osmosis_control_msgs::Goal target_;
	sensor_msgs::LaserScan scan_;
	//nav_msgs::Odometry odom_;

	double obstacle_distance;
	double safety_distance;
	double nu;
	double psi;

  void callbackGoal(const osmosis_control_msgs::Goal & thegoal)
  {
  	target_=thegoal;
  	ROS_INFO("GOAL : x: [%f], y:[%f]",target_.point.x,target_.point.y);
  }

  void callbackScan(const sensor_msgs::LaserScan & thescan)
  {
  	scan_=thescan;
  }

  void callbackPose(const geometry_msgs::Pose2D & msg)
  {
  	robot_pose_ = msg;
  }

public:
	OsmosisControl() : osmosis::Control()
  {
    //set up the publishers and subscribers
  	cmd_vel_pub_   = nh_.advertise<geometry_msgs::Twist>("cmd_vel", 1);
  	goal_reach_pub_= nh_.advertise<std_msgs::Bool>("target_reached", 10);
  	scan_sub_ = nh_.subscribe("scan", 1, &OsmosisControl::callbackScan, this);
  	goal_sub_ = nh_.subscribe("target", 1, &OsmosisControl::callbackGoal, this);
  	odom_sub_ = nh_.subscribe("pose", 1, &OsmosisControl::callbackPose, this);
  	//initialization of attributes
  	freq_=10;
		//
		target_.point.x = std::numeric_limits<double>::infinity();
		target_.point.y = std::numeric_limits<double>::infinity();
		// params
		nh_.param<double>("obstacle_distance", obstacle_distance, 0.9);
		nh_.param<double>("safety_distance", safety_distance, 0.9);
		nh_.param<double>("nu", nu, 0.1);
		nh_.param<double>("psi", psi, 0.1);
  }

	bool run()
  {
  	ros::Rate loop_rate(freq_); //using 10 makes the robot oscillating trajectories, TBD check with the PF algo
  	while (nh_.ok())
  	{
      auto prev_state = get_state();
			step(robot_pose_, target_, scan_,
        obstacle_distance, nu, psi, safety_distance);
      auto state = get_state();
			if (state != prev_state)
				ROS_INFO_STREAM("Control state:" << get_state_str());
      if (get_state() == Control::State::ARRIVED_GOAL) {
        std_msgs::Bool a;
      	a.data=true;
      	goal_reach_pub_.publish(a);
				ROS_INFO("Arrived");
      }
  		cmd_vel_pub_.publish(command);
  		ros::spinOnce(); // Need to call this function often to allow ROS to process incoming messages
  		loop_rate.sleep(); // Sleep for the rest of the cycle, to enforce the loop rate
  	}
  	return true;
  }

}; // end of class

////////////////////// MAIN //////////////////////

int main(int argc, char** argv)
{
	//init the ROS node
	ros::init(argc, argv, "osmosis_control_node");
	OsmosisControl myOsmosisControl;
	//launch the control node
	myOsmosisControl.run();
}
