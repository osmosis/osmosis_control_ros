/*
* Copyright 2018 LAAS-CNRS
*
* This file is part of the OSMOSIS project.
*
* Osmosis is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 3 as
* published by the Free Software Foundation.
*
* Osmosis is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
*/

#include <iostream>

#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Pose.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Pose2D.h>
#include <tf/transform_broadcaster.h>
#include <std_msgs/Bool.h>

using namespace std;

class Localization
{
private:
	ros::NodeHandle nh_;
	double freq_;
	ros::Publisher pose_pub_;
	ros::Subscriber odom_sub_, fault_sub_;
	nav_msgs::Odometry odom_;
	geometry_msgs::Pose2D robot_pose_;
	bool fault_not_updated_;

public:
	void localizationCallbackOdom(const nav_msgs::Odometry::ConstPtr& msg) {
		tf::Pose pose;
		tf::poseMsgToTF(msg->pose.pose, pose);
		/* ROS_INFO("Seq: [%d]", msg->header.seq);
		ROS_INFO("Position-> x: [%f], y: [%f], z: [%f]", msg->pose.pose.position.x,msg->pose.pose.position.y, msg->pose.pose.position.z);
		ROS_INFO("Orientation-> x: [%f], y: [%f], z: [%f], w: [%f]", msg->pose.pose.orientation.x, msg->pose.pose.orientation.y, msg->pose.pose.orientation.z, msg->pose.pose.orientation.w);
		ROS_INFO("Vel-> Linear: [%f], Angular: [%f]", msg->twist.twist.linear.x,msg->twist.twist.angular.z);
		*/
		robot_pose_.x = msg->pose.pose.position.x;
		robot_pose_.y = msg->pose.pose.position.y;
		robot_pose_.theta =  tf::getYaw(pose.getRotation());
	}

	void callbackFaultInjection(const std_msgs::Bool& fault) {
		fault_not_updated_ = fault.data;
	}

	bool run() {
		ros::Rate loop_rate(freq_);
		while (nh_.ok())
		{
			if (!fault_not_updated_)
				pose_pub_.publish(robot_pose_);
			ros::spinOnce(); // Need to call this function often to allow ROS to process incoming messages
			loop_rate.sleep(); // Sleep for the rest of the cycle, to enforce the loop rate
		}
		return true;
	}

	Localization() : freq_(10), fault_not_updated_(false) {
		pose_pub_ = nh_.advertise<geometry_msgs::Pose2D>("pose", 1);
		odom_sub_ = nh_.subscribe("odom", 1, &Localization::localizationCallbackOdom, this);
		fault_sub_ = nh_.subscribe("fault", 1, &Localization::callbackFaultInjection, this);
	}

};

int main(int argc, char** argv)
{
	//init the ROS node
	ros::init(argc, argv, "localization_node");
	Localization mylocalization;
	mylocalization.run();
}
